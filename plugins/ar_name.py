#!/usr/bin/env python3

import secrets
from argon.command import Command


cons_prob = {'t':16587, 'n': 12666, 's':11450, 'r': 10977, 'h':10795,
             'd':7874, 'l':7253, 'c':4943, 'm':4761, 'f':4200,
             'y':3853, 'w':3819, 'g':3693, 'p':3316, 'b':2715,
             'v':2019, 'k':1257, 'x':315, 'qu':205, 'j':188,
             'z':128,
             '':40000}

vowl_prob = {'e':21912, 'a':14810, 'o':14003, 'i':13318, 'u':5246,
             '':40000}

def randnum(num):
    return secrets.randbelow(num)

def randbool():
    return bool(secrets.randbits(1))

def build_consonant():
    consonant = None
    while consonant is None:
        index = randnum(22)
        prob = list(cons_prob.values())[index]
        if prob > randnum(40000):
            consonant = list(cons_prob.keys())[index]
    return consonant

def build_vowel():
    vowel = ""
    length = randnum(2) + 1
    while len(vowel) < length:
        index = randnum(6)
        prob = list(vowl_prob.values())[index]
        if prob > randnum(40000):
            vowel += list(vowl_prob.keys())[index]
    if vowel == "ii":
        vowel = "i"
    elif vowel == "uu":
        vowel = "u"
    elif vowel == "aa":
        vowel = "a"
    return vowel

def build_word():
    word = ""
    count = 0
    while randbool() or len(word) < 2:
        if count > 4: break
        word += build_consonant()
        word += build_vowel()
        if randnum(3) > 0:
            word += build_consonant()
        count += 1
    return word

def generate_word(match, metadata, bot):
    words = match.group("count")
    buffer = ""
    if words != None:
        for i in range(int(words)-1):
            buffer += build_word()
            buffer += ", "
    buffer += build_word()

    bot.reply(buffer, metadata["message_id"], metadata['from_group'], metadata['_id'])

def register_with(argon):
    argon.add_commands(
        Command(r"{ident}name(?: (?P<count>\d+))?",
                "name (<number of names>)",
                "Generate random, pronounciable names.",
                generate_word),
    )
