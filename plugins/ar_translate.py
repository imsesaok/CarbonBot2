from mtranslate import translate
from argon.command import Command

def query(match, metadata, bot):
    original_text = match.group('string')
    dest_lang = match.group('dest')
    bot.reply(translate(original_text, dest_lang, "auto"), metadata["message_id"],
              metadata['from_group'], metadata['_id'])

def register_with(argon):
    argon.add_commands(
        # Instant Answers
        Command(r">(?P<dest>\w\w\w?) (?P<string>.+)", "><destination language> <text>",
                "Translate a sentence into specified language using Google Translate.", query),
    )
