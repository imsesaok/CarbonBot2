import base64
from datetime import datetime
import logging
import re
import socket
import ssl
import threading
import time
import queue
from argon.adapter import Adapter
from argon.constants import SOFTWARE_NAME, VERSION, DESCRIPTION_AND_AUTHORS


class IRCAdapter(Adapter):
    def __init__(self, server, port, is_ssl, channels, owner, nick="Argon", codec="UTF-8", is_sasl=False, password=None, identifier="!", nick_as_identifier=True):
        super(IRCAdapter, self).__init__(identifier=identifier)
        self.server = server
        self.port = port
        self.is_ssl = is_ssl
        self.is_sasl = is_sasl
        self.orig_nick = nick
        self.nick = nick
        self.channels = channels
        self.owner = owner
        self.codec = codec
        self.password = password

        self.PRIVMSG_re = re.compile(
            r':(?P<nick>[^\s]+)![^\s]+@[^\s]+ PRIVMSG (?P<chan>[^\s]+) :(?P<msg>.*)')

        threading.Thread.__init__(self)
        self.logger = logging.getLogger("IRCAdapter")
        logging.basicConfig(level=logging.DEBUG)

    def ping(self, msg):
        pong = msg.lstrip("PING :")
        self.raw_send("PONG :{}\n".format(pong))

    def register_callback(self, func, _id):
        self.execute = func
        self._id = _id

    def raw_send(self, message):
        self.sock.send(message.encode(self.codec))
        self.logger.info("→ {}".format(message.rstrip()))

    def send(self, message, to, command="NOTICE"):
        if not message.strip("\r\n").strip():
            return
        for line in message.split("\n"):
            message = line.strip(" ")
            self.raw_send(command + " " + to + " :" + message + "\n")

    def reply(self, message, to, group):
        return self.send(to+": "+message, group)

    def join_channel(self, ch):
        self.raw_send("JOIN %s\n" % ch)

    def handle_message(self, chan, user, message):
        is_pm = chan == user

        if is_pm and message.upper() == "\x01VERSION\x01":
            self.logger.info("{} queried our version".format(user))
            self.send("\x01VERSION {}:{}:\x01".format(SOFTWARE_NAME, VERSION), chan, command="NOTICE")
            return

        if not is_pm:
            # Check if the message was sent through a bridging bot

            # First we check the sender's nick
            m_user = re.fullmatch(r'[^a-zA-Z]+|apiaceae|xxxx_', user)
            if m_user:

                # Then we check the message. Also support color codes
                m_message = re.fullmatch(r'(?:\x03[0-9]{2}|[ \x01-\x0f])*<([^>]+)>[ \x01-\x0f]*:[ \x01-\x0f]*(.+)', message)
                #                                                         ↑         ↑
                #                                                         |         `- blanket match IRC formatting reset codes
                #                                                         `- user name. formatting codes are stripped out later

                if m_message:
                    # Move information so that we think this message came from that user
                    # Append [proxy] to avoid impersonation
                    bridge_nick = user

                    # Remove formatting codes and zero-width spaces (U+200B) from the user name part
                    bridged_user = re.sub(r'\x03[0-9]{2}|[\x01-\x0f]|​', "", m_message[1])

                    if bridge_nick == "apiaceae":
                        user = "@{} [{}]".format(bridged_user, bridge_nick)
                    else:
                        user = "{}[{}]".format(bridged_user, bridge_nick)
                    message = m_message[2]

            self.logger.info("Received message in ``{}'' from ``{}'': ``{}''".format(chan, user, message))

        else:
            self.logger.info("Received PM from ``{}'': ``{}''".format(user, message))

        # In PMs, using an identifier is optional
        ident = self.identifier_optional if is_pm else self.identifier

        metadata = {"from_user": user, "from_group": chan, "when": datetime.now(),
                    "_id": self._id, "ident": ident, "type": self.__class__.__name__,
                    # FIXME: Check for other admins in the channel.
                    "mentioned": self.nick in message, "is_mod": user == self.owner,
                    "message_id": user, "pm": is_pm}
        self.execute(message, metadata)

    def _recv_next_line(self):
        next_line = None

        no_recv_amount_threshold = 100000
        no_recv_time_treshold = 2.0

        i = 0
        no_recv_start = time.time()
        while next_line is None:
            try:
                next_line = self.recv_queue.get_nowait()
            except queue.Empty:
                incoming = self.sock.recv(2048).decode(self.codec)

                got_new = False
                for line in incoming.split("\n"):
                    line = line.strip()
                    if line:
                        self.recv_queue.put(line)
                        got_new = True

                if not got_new:
                    # Safety net to avoid looping
                    i += 1
                    if i >= no_recv_amount_threshold and time.time() - no_recv_start < no_recv_time_treshold:
                        self.logger.error("Receiving loop has spun %d times in under %f seconds without receiving content, aborting adapter", i, no_recv_time_treshold)
                        return None

        self.logger.info("← {}".format(next_line))
        return next_line

    def run(self):
        raw_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        raw_socket.connect((self.server, self.port))  # Create & connect socket

        self.recv_queue = queue.Queue()

        try:
            if self.is_ssl:
                # Optionally wrap socket with SSL
                self.sock = ssl.wrap_socket(raw_socket)
            else:
                self.sock = raw_socket

            authenticated = False
            if self.is_sasl:
                # SASL request has to be sent before everything else.
                self.raw_send("CAP REQ :sasl\n")

            self.raw_send("NICK %s\n" % self.nick)
            self.raw_send("USER {0} {0} {0} :{1}, {2}\n".format(self.nick, SOFTWARE_NAME, DESCRIPTION_AND_AUTHORS))

            self.logger.info("Logging in...")

            # Client Capability Negotiation loop
            while True:
                msg = self._recv_next_line()

                # None signals that the receiving loop spinned to fast, and we should abort
                if msg is None:
                    return

                # Remove nick from msg to avoid conflicts with IRC keywords
                msg = msg.replace(self.nick, "")

                if "MODE %s" % self.nick in msg or "MOTD" in msg:
                    self.logger.info("Okay, server doesn't want to do capability negotiation, continuing")
                    break

                elif msg.startswith("PING :"):
                    self.ping(msg)

                # Nickname already in use
                elif " 433 " in msg:
                    self.nick += "_"
                    self.raw_send("NICK %s\n" % self.nick)

                elif self.is_sasl and "CAP " in msg and " ACK :sasl" in msg:
                    self.raw_send("AUTHENTICATE PLAIN\n")

                elif self.is_sasl and "AUTHENTICATE" in msg:
                    auth = ('{sasl_username}\0'
                            '{sasl_username}\0'
                            '{sasl_password}').format(sasl_username=self.orig_nick, sasl_password=self.password)
                    auth = base64.encodestring(auth.encode(self.codec))
                    auth = auth.decode(self.codec).rstrip('\n')
                    self.raw_send("AUTHENTICATE " + auth + "\n")

                # Auth succeeded; end Client Capability Negotiation phase
                elif self.is_sasl and " 903 " in msg:
                    authenticated = True
                    self.logger.info("Authentication confirmed by server, exiting CAP loop")
                    self.raw_send("CAP END\n")
                    break

                elif self.is_sasl and (" 904 " in msg or " 905 " in msg):
                    self.logger.warning("Authentication failed, shutting down IRC adapter")
                    return

                elif "NOTICE " in msg or re.search(r"^:[^ ]* 900", msg):
                    pass

                elif re.search(r"^:[^ ]* 00[1-5]", msg):
                    self.logger.info("Server sends replies in range 1 through 5, so it considers the CAP loop done")
                    break

                else:
                    self.logger.info("Unexpected reply, ignoring")

            if self.is_sasl:
                if not authenticated:
                    self.logger.error("Authentication has not completed successfully! Aborting adapter")
                    return

            self.logger.info("Joining channels")
            for ch in self.channels:
                self.join_channel(ch)

            self.logger.info("Connection set up and CAP done")

            self._on_initialized(self.owner)

            while True:
                try:
                    msg = self._recv_next_line()

                    # None signals that the receiving loop spinned to fast, and we should abort
                    if msg is None:
                        return

                    if msg.startswith("PING :"):
                        self.ping(msg)
                        continue

                    m = self.PRIVMSG_re.fullmatch(msg)
                    if not m:
                        continue

                    sender = m.group("nick")
                    chan = m.group("chan")

                    # Handle PMs: set the channel name to the name of the sender
                    # This will make sure replies are sent to the correct place
                    if chan == self.nick:
                        chan = sender

                    self.handle_message(chan, sender, m.group("msg"))

                except Exception:
                    self.logger.error("Error while reading socket.", exc_info=True)

        finally:
            try:
                raw_socket.close()
            except Exception:
                self.logger.warning("Failed to close socket", exc_info=True)
