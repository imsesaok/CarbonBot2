from datetime import datetime
import threading
from argon.adapter import Adapter

class ConsoleAdapter(Adapter):
    def __init__(self, nick="Argon"):
        super(ConsoleAdapter, self).__init__()
        self.nick = nick

        threading.Thread.__init__(self)

    def run(self):
        try:
            while True:
                print('>', end=' ')
                message = input()
                metadata = {"from_user": "console user", "from_group": "console", "when": datetime.now(),
                            "_id": self._id, "ident": self.identifier, "type": self.__class__.__name__,
                            "mentioned": self.nick in message, "is_mod": True, "message_id": message, }
                self.callback(message, metadata)
        except EOFError:
            print("^D\nGoodbye.")
            pass

    def register_callback(self, func, _id):
        self.callback = func
        self._id = _id

    def send(self, message, group):
        for line in message.split("\n"):
            msg = line.strip(" ")
            print('< {msg}'.format(to=group, msg=msg))

    def reply(self, message, to, group):
        self.send(message, group)
