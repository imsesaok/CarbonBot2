import threading, re
from argon.adapter import Adapter


class TelegramAdapter(Adapter):
    def __init__(self, token, owner, owner_id, identifier="[!/]"):
        super(TelegramAdapter, self).__init__(identifier=identifier)
        from telegram.ext import Updater
        self.updater = Updater(token=token)
        self.dispatcher = self.updater.dispatcher
        self.bot = self.updater.bot
        self.bot_id = self.bot.get_me()["username"]
        self.owner = owner
        self.owner_id = owner_id
        threading.Thread.__init__(self)

    def run(self):
        from telegram.ext import MessageHandler, Filters
        handler = MessageHandler(Filters.text | Filters.command, self.eval)
        self.dispatcher.add_handler(handler)
        self.updater.start_polling()
        self._on_initialized(self.owner_id)

    def register_callback(self, func, _id):
        self.callback = func
        self._id = _id

    def eval(self, bot, update):
        message = update.message.text.strip().replace("@"+self.bot_id, "")
        if re.search(r"^/\w+@\w+", message): return
        if "^" in message:
            try:
                message = message.replace("^", update.message.reply_to_message.text.strip())
            except AttributeError:
                pass
        from_user = update.message.from_user.name
        from_group = update.message.chat_id
        message_id = update.message.message_id
        when = update.message.date
        user_stat = bot.get_chat_member(
            from_group, update.message.from_user.id).status
        from telegram import ChatMember
        is_mod = user_stat == ChatMember.ADMINISTRATOR or user_stat == ChatMember.CREATOR
        metadata = {"from_user": from_user, "from_group": from_group, "when": when,
                    "_id": self._id, "ident": self.identifier, "type": self.__class__.__name__,
                    # FIXME: Check for other admins in the channel.
                    "mentioned": "@"+self.bot_id in message, "is_mod": from_user == self.owner or is_mod,
                    "message_id": message_id, }
        self.callback(message, metadata)

    def send(self, message, group):
        self.bot.send_message(chat_id=group, text=message)

    def reply(self, message, to, group):
        self.bot.send_message(chat_id=group, text=message,
                              reply_to_message_id=to)
