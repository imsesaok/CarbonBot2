import sys
import re
from argon import plugin_loader
import argon.constants as constants

# Commands
from argon.command import Command
# Adapters
from argon.adapter import Adapter
from argon.adapter.telegram import TelegramAdapter


_MAGIC_SRC_URL_VALUE = "source provided in another way"


class Argon:
    def __init__(self, adapters, source_url, privacy_policy_url=None, commands=None):
        if not source_url:
            raise ValueError(
                "Since this bot is released under the AGPL, you must inform users where they can "
                "find the source code for your version (including your modifications). There's a "
                "built-in mechanism for this, which you are encouraged to use. See the "
                "Configuration section of the README. To disable this check, you can set the URL "
                'to the exact value "{}" (without the quotes).'.format(_MAGIC_SRC_URL_VALUE)
            )
        self.SOURCE_URL = None if source_url == _MAGIC_SRC_URL_VALUE else source_url
        self.PRIVACY_POLICY_URL = privacy_policy_url
        self.SOFTWARE_NAME = constants.SOFTWARE_NAME
        self.VERSION = constants.VERSION
        self.DESCRIPTION = constants.DESCRIPTION
        self.AUTHORS = constants.AUTHORS

        self.commands = commands if commands is not None else []
        self.adapters = adapters
        self.metadata = dict()
        for _id in self.adapters:
            self.adapters[_id].register_callback(self.process, _id)

    def add_commands(self, *commands):
        self.commands += commands

    def run(self):
        for adapter in self.adapters.values():
            adapter.start()

    def process(self, message, metadata):
        found_match = False

        for command in self.commands:
            if command.raw_match:
                match = message if command.regex == message else None
            else:
                regex = command.regex.format(ident=metadata["ident"])
                match = re.search("^{}$".format(regex), message)

            if match is not None:
                found_match = True
                command.on_exec(match, {**metadata, **self.metadata}, self)


        if not found_match and re.match(metadata["ident"], message):
            if not isinstance(self.adapters[metadata["_id"]], TelegramAdapter):
                self.reply('Command not recognized. Use the command help for help.',
                           metadata["message_id"], metadata['from_group'], metadata['_id'])

    def send(self, message, group, _id):
        self.adapters[_id].send(message, group)

    def reply(self, message, to, group, _id):
        self.adapters[_id].reply(message, to, group)

    def finalise(self):
        for adapter in self.adapters.values():
            adapter.finalise()
